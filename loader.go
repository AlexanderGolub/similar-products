package main

import (
	"os"
	"bufio"
	"fmt"
	"strings"
)
type (
	Product struct {
		Code string `json:"code"`
		Name string `json:"name"`
	}
)
func LoadProducts(filePath string) []Product {
	f, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	var products []Product
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		splitedString:= strings.Split(scanner.Text(), "%%")
		var prod Product
		prod.Code = splitedString[0]
		prod.Name = splitedString[1]
		products = append(products, prod)
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, err)
	}

	return products
}