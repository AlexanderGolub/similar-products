package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const (
	Addr string = ":8088"
)
type Products struct {
	Word1 string `json:"word1"`
	Word2 string `json:"word2"`
}

type appHandler func(http.ResponseWriter, *http.Request) (int, error)

func (fn appHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if status, err := fn(w, r); err != nil {
		switch status {

		case http.StatusInternalServerError:
			http.Error(w, http.StatusText(http.StatusInternalServerError),
				http.StatusInternalServerError)
		default:
			// Перехватываем остальные ошибки, для которых
			// нет специальных обработчиков
			http.Error(w, http.StatusText(http.StatusInternalServerError),
				http.StatusInternalServerError)
		}
	}
}

func DefaultHeaders(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
}

func RouteHandler(w http.ResponseWriter, r *http.Request) (int, error) {
	// setting default headers
	DefaultHeaders(w)
	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return http.StatusMethodNotAllowed, nil
	}
	
	var products Products
	body, _ := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err := json.Unmarshal(body, &products); err != nil {
		return http.StatusInternalServerError, err
	}

	score := ScoreSimilar(products.Word1, products.Word2)
	//var text string = `{"similarity" : "` + string(score) + `"}`
	fmt.Fprint(w, `{"similarity" : "`, score, `"}`)
	return http.StatusOK, nil
}



func main() {

	http.Handle("/", appHandler(RouteHandler))
	http.ListenAndServe(Addr, nil)

}
