package main

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestMin(t *testing.T) {
	r := require.New(t)

	min:= Min(1, 3)
	r.Equal(min, 1, "minimum should be 1")
	r.NotEqual(min, 3, "minimum shouldn't be 1")

	min2:= Min(3, 1)
	r.Equal(min2, 1, "minimum should be 1")
	r.NotEqual(min2, 3, "minimum shouldn't be 1")
}

func TestPrepareString(t *testing.T) {
	r := require.New(t)

	illegalChars := []string {";", "/", "\\", ":", " "}
	legalChars := "|"

	inputString := "HJgf : ; dksjfk ,. "

	resultString := PrepareString(inputString)

	r.NotContains(resultString, illegalChars, "output string mustn't have a symbol ;")
	r.Contains(resultString, legalChars, "output should contents | symbol")

}

func TestStringToSlice(t *testing.T) {
	a := assert.New(t)
	inputString1 := "word1|word2"
	slice:= StringToSlice(inputString1)
	a.Len(slice, 2,"should be slice of strings")

	inputString2 := "word1"
	slice2:= StringToSlice(inputString2)
	a.Len(slice2, 1, "should be string type")

}

func TestStemWord(t *testing.T) {
	inputWord := "корабль"
	stemmedWord, _ := StemWord(inputWord)
	require.Equal(t, "корабл", stemmedWord, "should be корабл")
}

func TestScoreSimilar(t *testing.T) {
	r:= require.New(t)
	inputString1 := "ожерелье золотое кукушка"
	inputString2 := "золотое кукушка ожерелье"

	score:= ScoreSimilar(inputString1, inputString2)
	r.Equal(100, score, "should be 100% similar")

}