package main

import (
	"github.com/kljensen/snowball"
	"strings"

)

func StemWord(word string) (string, error) {
	stemmed, err := snowball.Stem(word, "russian", true)
	return stemmed, err
}

func PrepareString(str string) string {
	str = strings.ToLower(str)
	r:= strings.NewReplacer(" ", "|", ",", "", ".", "", "/", "", "\\", "", ";", "", ":", "")
	str = r.Replace(str)
	for i := 0; i <= 10; i++ {
		r2:= strings.NewReplacer("||", "|")
		str = r2.Replace(str)
	}

	return strings.TrimSpace(str)
}

func StringToSlice(str string) []string {
	return strings.Split(PrepareString(str), "|")
}

func Min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

func ScoreSimilar(str1 string, str2 string) int {
	var score int = 0
	arrStr1 := StringToSlice(str1)
	arrStr2 := StringToSlice(str2)
	for _, elem1 := range arrStr1 {
		stemmedStr1, err1 := StemWord(elem1)
		if err1 != nil {
			continue
		}

		for _, elem2 := range arrStr2 {
			stemmedStr2, err2 := StemWord(elem2)
			if err2 != nil {
				continue
			}
			if stemmedStr1 == stemmedStr2 {
				score += 1
			}
		}
	}
	if score > 100 {
		score = 100
	}
	if score < 0 {
		score = 0
	}

	score = int(float64(score) / float64(Min(len(arrStr1), len(arrStr2))) * 100)
	return score
}
